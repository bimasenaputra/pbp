from django.db import models

class Note(models.Model):
    receiver = models.CharField(max_length=30)
    sender = models.CharField(max_length=30)
    title = models.CharField(max_length=100)
    message = models.TextField()