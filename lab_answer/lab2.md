## 1. Apakah perbedaan antara JSON dan XML?
### Bahasa
JSON: JavaScript.&nbsp;
XML: Bukan bahasa pemrograman, melainkan hanya bahasa markup.
### Format penulisan
JSON: Menggunakan key-value pairs.&nbsp;
XML: Menggunakan tags.
### Tipe Data
JSON: String, number, boolean, null, object, dan array.&nbsp;
XML: String, chart, image, dan object non-primitive lainnya.
### Namespace, Comment, dan Metadata
JSON: Tidak ada.&nbsp;
XML: Ada.
### Ukuran Dokumen
JSON: Tipis.&nbsp;
XML: Tebal.
### Dukungan Standar W3C
JSON: JSON-LD.&nbsp;
XML: XML Query, XPath, XML Schema, XSLT, and XForms.

Referensi:
https://hackr.io/blog/json-vs-xml&nbsp;
https://www.javatpoint.com/json-vs-xml&nbsp;
https://www.techwell.com/techwell-insights/2020/04/comparing-xml-and-json-what-s-difference

## 2. Apakah perbedaan antara HTML dan XML?
### Sensitivitas Huruf
HTML: Tidak sensitif.&nbsp;
XML: Sensitif.
### Fungsi
HTML: Visualisasi data.&nbsp;
XML: Transfer data.
### Nesting
HTML: Tidak harus benar.&nbsp;
XML: Harus benar.
### Namespace dan Whitespace
HTML: Tidak.&nbsp;
XML: Ya.
### Jenis Tags
HTML: Telah didefinisikan.&nbsp;
XML: Didefinisikan sendiri oleh pengguna.
### Tag Penutup
HTML: Tidak wajib.&nbsp;
XML: Wajib.

Referensi:
https://www.guru99.com/xml-vs-html-difference.html&nbsp;
https://www.geeksforgeeks.org/html-vs-xml/&nbsp;
https://mythemeshop.com/blog/xml-vs-html/#c
