import 'package:flutter/material.dart';

void main() {
  runApp(const MaterialApp(
    title: "Belajar Form Flutter",
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  const BelajarForm({Key? key}) : super(key: key);

  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController input = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: const Text("Lab 7"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: [
                Container(
                  alignment: Alignment.center,
                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                  child: const Text(
                    'Ask The Teacher',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                    width: 300,
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.bottomLeft,
                          padding: const EdgeInsets.fromLTRB(0, 0, 0, 1),
                          child: const Text(
                            'Question',
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          child: TextFormField(
                            textAlignVertical: TextAlignVertical.top,
                            maxLines: 7,
                            controller: input,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(0.0)),
                              ),
                              hintText: "Enter your suggestions or critics",
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'This field can\'t be empty';
                              }
                              return null;
                            },
                          ),
                        ),
                      ],
                    )),
                Container(
                  alignment: Alignment.center,
                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                  child: ElevatedButton(
                    child: const Text(
                      "Submit Question",
                      style: TextStyle(color: Colors.white),
                    ),
                    style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            Colors.deepPurple)),
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        print(input.text);
                        input.clear();
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
