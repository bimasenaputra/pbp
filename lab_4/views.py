from django.shortcuts import render
from django.http import HttpResponseRedirect
from lab_2.models import Note
from .forms import NoteForm

def index(request):
    return render(request, 'lab4_index.html', {'notes':Note.objects.all()})

def add_note(request):
	if request.method == 'POST':
		form = NoteForm(request.POST or None)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect("/lab-4")
	else:
		form = NoteForm(request.POST or None)
	response = {'notes':form}
	return render(request, "lab4_form.html", response)

def note_list(request):
    return render(request, 'lab4_note_list.html', {'notes':Note.objects.all()})