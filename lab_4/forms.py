from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['receiver', 'sender', 'title', 'message']
        widgets = {
            'receiver': forms.TextInput(attrs={
                'class':"form-control",
                'syle':'max-width: 300px;',
                'placeholder': 'Receiver\'s name'
                }),
            'sender': forms.TextInput(attrs={
                'class':"form-control",
                'syle':'max-width: 300px;',
                'placeholder': 'Sender\'s name'
                }),
            'title': forms.TextInput(attrs={
                'class':"form-control",
                'syle':'max-width: 300px;',
                'placeholder': 'Note title'
                }),
            'message': forms.TextInput(attrs={
                'class':"form-control",
                'syle':'max-width: 300px;',
                'placeholder': 'Note message'
                })
        }