from django.db import models
from django.utils import timezone
from datetime import datetime, date

class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.BigIntegerField(max_length=10)
    birth_date = models.DateField(default=timezone.now)