import 'package:flutter/material.dart';

import 'package:flutter_complete_guide/dummy_data.dart';
import 'package:flutter_complete_guide/models/meal.dart';
import 'package:flutter_complete_guide/widgets/main_drawer.dart';

class Database extends StatefulWidget {
  static const routeName = '/database';
  @override
  _DBState createState() => _DBState();
}

class _DBState extends State<Database> {
  int _rowsPerPage = 10;
  int _currentSortColumn = 0;
  bool _isAscending = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Meals Database'),
      ),
      drawer: MainDrawer(),
      body: Container(
        width: double.maxFinite,
        padding: const EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: PaginatedDataTable(
            onRowsPerPageChanged: (v) {
              setState(() {
                _rowsPerPage = v;
              });
            },
            availableRowsPerPage: [5, 10, 25, 50, 100],
            rowsPerPage: _rowsPerPage,
            sortColumnIndex: _currentSortColumn,
            sortAscending: _isAscending,
            columns: [
              DataColumn(
                label: Text('Id',style: Theme.of(context).textTheme.bodyText2,),
                onSort: (columnIndex, _) {
                    setState(() {
                      _currentSortColumn = columnIndex;
                      if (_isAscending) {
                        _isAscending = false;
                        DUMMY_MEALS
                            .sort((a, b) => a.id.compareTo(b.id));
                      } else {
                        _isAscending = true;
                        DUMMY_MEALS
                            .sort((a, b) => b.id.compareTo(a.id));
                      }
                    });
                  }),
              DataColumn(
                label: Text(
                  'Name',style: Theme.of(context).textTheme.bodyText2,
                ),
                onSort: (columnIndex, _) {
                    setState(() {
                      _currentSortColumn = columnIndex;
                      if (_isAscending) {
                        _isAscending = false;
                        DUMMY_MEALS
                            .sort((a, b) => a.title.compareTo(b.title));
                      } else {
                        _isAscending = true;
                        DUMMY_MEALS
                            .sort((a, b) => b.title.compareTo(a.title));
                      }
                    });
              }),
              DataColumn(
                  label: Text(
                    'Duration', style: Theme.of(context).textTheme.bodyText2,
                  ),
                  // Sorting function
                  onSort: (columnIndex, _) {
                    setState(() {
                      _currentSortColumn = columnIndex;
                      if (_isAscending) {
                        _isAscending = false;
                        DUMMY_MEALS
                            .sort((a, b) => a.duration.compareTo(b.duration));
                      } else {
                        _isAscending = true;
                        DUMMY_MEALS
                            .sort((a, b) => b.duration.compareTo(a.duration));
                      }
                    });
                  }),
              DataColumn(
                  label: Text(
                    'Complexity',
                    style: Theme.of(context).textTheme.bodyText2,
                  ),
                  // Sorting function
                  onSort: (columnIndex, _) {
                    setState(() {
                      _currentSortColumn = columnIndex;
                      if (_isAscending) {
                        _isAscending = false;
                        DUMMY_MEALS.sort((productA, productB) => productB
                            .complexity.index
                            .compareTo(productA.complexity.index));
                      } else {
                        _isAscending = true;
                        DUMMY_MEALS.sort((productA, productB) => productA
                            .complexity.index
                            .compareTo(productB.complexity.index));
                      }
                    });
                  }),
              DataColumn(
                  label: Text(
                    'Affordability',
                    style: Theme.of(context).textTheme.bodyText2,
                  ),
                  // Sorting function
                  onSort: (columnIndex, _) {
                    setState(() {
                      _currentSortColumn = columnIndex;
                      if (_isAscending == true) {
                        _isAscending = false;
                        DUMMY_MEALS.sort((productA, productB) => productB
                            .affordability.index
                            .compareTo(productA.affordability.index));
                      } else {
                        _isAscending = true;
                        DUMMY_MEALS.sort((productA, productB) => productA
                            .affordability.index
                            .compareTo(productB.affordability.index));
                      }
                    });
                  }),
            ],
            columnSpacing: 100,
            horizontalMargin: 10,
            showCheckboxColumn: false,
            source: Data(DUMMY_MEALS),
          ),
        ),
      ),
    );
  }
}

class Data extends DataTableSource {
  Data(this._data);

  final List<Meal> _data;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => _data.length;

  @override
  int get selectedRowCount => 0;

  @override
  DataRow getRow(int index) {
    return DataRow(cells: [
      DataCell(Text(_data[index].id)),
      DataCell(Text(_data[index].title)),
      DataCell(Text('${_data[index].duration}')),
      DataCell(Text(() {
        if (_data[index].complexity == Complexity.Simple)
          return 'Simple';
        else if (_data[index].complexity == Complexity.Challenging)
          return 'Challenging';
        else if (_data[index].complexity == Complexity.Hard)
          return 'Hard';
        else
          return 'Unknown';
      }())),
      DataCell(Text(() {
        if (_data[index].affordability == Affordability.Affordable)
          return 'Affordable';
        else if (_data[index].affordability == Affordability.Pricey)
          return 'Pricey';
        else if (_data[index].affordability == Affordability.Luxurious)
          return 'Luxurious';
        else
          return 'Unknown';
      }())),
    ]);
  }
}
